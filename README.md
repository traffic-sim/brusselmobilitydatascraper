# Introduction

This project contains Python scripts useful for converting Brussels datasets. The Brussels data is divided into two
types: OpenData (available
at [https://data-mobility.brussels/traffic/api/counts/](https://data-mobility.brussels/traffic/api/counts/)) and ANPR
counting data provided by Brussels Mobility. This project includes the following scripts:

- **ANPRAllDayExtractor.py**: Reads ANPR data and saves the data into separate files, one for each day.
- **ANPRDatasetConverter**: Converts the dataset in CSV format for ANPR counting into a format that can be used to
  calibrate traffic models for SUMO.
- **BXLOpenDataScraper**: Queries the OpenData service of Brussels Mobility and saves the traffic counts to a file.
- **BXLOpenDataToNamur**: Converts OpenData into the same format as the data provided by Macq for the traffic count data
  of Namur.

# Usage

To convert ANPR counts for use with calibrators (RS/SPSA/Novel), first configure the TOML as follows:

```
anpr_dataset_file = "raw_data.csv"
pbf_path = "bxl_map.pbf"
sumo_net_file = "bxl_map.net.xml"
out_folder = "out_bxl/2023-11-30"
op_type = "NOTRAINTEST"
nfolds = 3
aggr_freq = 900
days_list = "2023-11-30 2023-12-02 2023-12-03"
```

where:

- **anpr_dataset_file** is the input dataset in CSV format
- **pbf_path** is the path to the road network in PBF format (converted from OSM)
- **sumo_net_file** is full path to the SUMO road network file
- **out_folder** is the folder where the output files are saved
- **op_type** determines the type of dataset to produce as output. Three options are possible (case-sensitive):
    - ``NOTRAINTEST``: just convert the entire dataset and prepare it for being used with calibration tools
    - ``KFOLD``: split into train/test using the k-fold method (use nfold parameter)
    - ``TONAMUR``: convert to the same format of NAMUR dataset provided by Macq
- **nfolds**: when using ``KFOLD``, this specified the number of folds to split the dataset
- **aggr_freq**: aggregation interval for data
- **days_list**: list of dates in format YYYY-MM-DD, separated by 1 space. This is the list of days that must be
  extracted from the

Then use the following command:

```
python ANPRDatasetConverter.py config.toml
```

# Convert Brussels ANPR data to Namur Macq format:

```
python ANPRDatasetConverter.py config.toml
```

# Scrape data for 1 minute, getting traffic data aggregated at a frequency of 1 minute:

```
python BXLOpenDataScraper.py -o output/ -i 1 -t 1
```

# Scrape data for 1 hour, getting traffic data aggregated at a frequency of 1 minute:

```
python BXLOpenDataScraper.py -o output/ -i 1 -t 1 -H true
```

# Scrape data for 5 hour, getting traffic data aggregated at a frequency of 15 minute:

```
python BXLOpenDataScraper.py -o output/ -i 15 -t 5 -H true
```

# Extract specific days from datasets

The scripts ```DayExtractor.py``` and ```ANPRAllDayExtractor.py``` allows extracting specific days from the scraped
dataset.

```DayExtractor.py``` extract the rows of the Brussel mobility dataset related to a specific day

```ANPRAllDayExtractor.py``` splits the input Brussel mobility dataset into multiple files, each containing the data for
one
day

