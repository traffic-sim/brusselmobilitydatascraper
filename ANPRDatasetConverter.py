import argparse
import os
import re
import subprocess
import sys
import tempfile
from dataclasses import dataclass
import tomli
from typing import List
from geopy.distance import distance
import pickle
import dateutil.parser
import numpy as np
import pandas as pd
import sumolib
import tqdm
import xmltodict
from pandas.core.frame import DataFrame
from pyrosm.pyrosm import OSM
from shapely.geometry import Point
from sklearn.model_selection import KFold

import VirtualSensorsUtil


@dataclass
class EdgeDataAdditional:
    """
    This corresponds to an "EdgeData" definition in the edge data XML additional file
    """
    ID: str
    fname: str
    begin: int = 0
    end: int = 0
    excludeEmpty: bool = True


def resample_dataset(data_df: DataFrame, resample_str: str = '15min') -> DataFrame:
    """
    Resample the input dataframe to the specified aggregation time interval

    :param data_df: a DATAFRAME
    :param resample_str:
    :return: the resampled dataset
    """
    unique_edges = data_df['source.id'].unique()

    resampled_df = pd.DataFrame()
    for edge in tqdm.tqdm(unique_edges):
        xs = data_df.loc[data_df['source.id'] == edge].copy()
        xs.drop(["Unnamed: 0", "_aggregation_id", "lat", "lang", "source.id", "start_date"], axis=1,
                inplace=True)

        xs.set_index('_start_timestamp', inplace=True)
        rxs = xs.resample(resample_str).sum()
        rxs['source.id'] = [edge for _ in range(len(rxs.index))]
        rxs['_start_timestamp'] = rxs.index.to_list()
        rxs.reset_index(drop=True, inplace=True)
        resampled_df = pd.concat([resampled_df, rxs])
    return resampled_df


def extract_by_day(date_str, df):
    """

    :param date_str: Date in ISO8601 format (ex: 2023-10-28T15:06:15). Do NOT include 'Z' at the end
    :param df:
    :return:
    """
    df_tmp = df.copy(deep=True)
    df_tmp['_start_timestamp'] = pd.to_datetime(df_tmp['_start_timestamp'])
    date = dateutil.parser.isoparse(date_str).date()

    start_times = df_tmp['_start_timestamp'].to_list()
    df_tmp['start_date'] = list(map(lambda d: d.date(), start_times))

    return df_tmp.loc[df_tmp['start_date'] == date]


def get_all_days(df):
    start_times = df['_start_timestamp'].to_list()
    return list(map(lambda d: d.date(), start_times))


def dataset_to_table(df_day: pd.DataFrame) -> pd.DataFrame:
    if df_day.empty:
        print('DF EMPTY')
    sub_dfs = []
    for sensor in df_day['source.id'].unique():
        sub_df = df_day.loc[df_day['source.id'] == sensor]
        sub_df = sub_df.rename(columns={'_start_timestamp': 'ts'})
        sub_df.set_index('ts', inplace=True)
        sub_df.drop(["source.id"], axis=1, inplace=True)
        sub_df.columns = [sensor]
        sub_dfs.append(sub_df)
    df = pd.concat(sub_dfs, axis=1)
    df.fillna(0, inplace=True)
    return df


def get_detectors_beyond_lanes(net_path, det_add_fname) -> dict[str, str]:
    """
    Launch a simulation using the specified additional file (containing the sensors definition). If the simulation fails
    then it returns the list of sensors that are over the input road network. In this case, I parse the list of sensors
    over the network and return them
    :param net_path: the full PATH to the sumo road network
    :param det_add_fname: the full PATH to the XML file containing the sensors
    :return: a dict where key: name of the sensor (to remove), value: name of the lane
    """
    sumo = sumolib.checkBinary("sumo")
    opts = [sumo,
            "-n", net_path,
            '-b', str(0),
            '-e', str(1),
            '-a', det_add_fname]
    p = subprocess.Popen(opts, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    p.wait()
    if len(err) <= 0:
        return {}
    if b'Quitting (on error).' not in err.splitlines():
        return {}
    pattern = re.compile(b"Error: The position of e1Detector '(.*)' lies beyond the lane's '(.*)' end.")
    # Error: The position of e1Detector '298428751' lies beyond the lane's '298428751_0' end.
    match_dict = {}
    for i, line in enumerate(err.splitlines()):
        matches = pattern.findall(line)
        if matches:
            # key: name of the sensor, value: name of the lane
            match_dict[matches[0][0].decode()] = matches[0][1].decode()

    # by running the simulation, we generate the detector output file which is useless for the dataset
    os.remove(os.path.join(os.path.dirname(det_add_fname), "detector.out.xml"))
    return match_dict  # I think only the keys is enough


def write_sensors_definition_to_file(net_path, df_coords: pd.DataFrame, out_fname="det_add_fname.xml",
                                     sensors_aggr_frequency=900) -> dict:
    """
    This function does the following steps:
    1. use mapDetectors to map the coordinates of the sensors to the SUMO network
    2. launch a dummy simulation to detect the sensors that are over the road network, causing errors
    3. write to XML only the definition of the available sensors

    :param net_path: the full PATH to the sumo network
    :param df_coords: the DATAFRAME containing the coordinates of the traffic sensors (should have 'id','lon','lat')
    :param out_fname: the outut file where the coordinates of the sensors are written (in CSV)
    :param sensors_aggr_frequency: the aggregation frequency of the sensors (in SECONDS)
    :return:
    """
    # Phase 1. Use mapDetectors to map the sensors coordinate with the SUMO edges
    f = tempfile.NamedTemporaryFile(suffix='.csv')
    df_coords.to_csv(f, sep=';')
    opts = ['python', '{}/tools/detector/mapDetectors.py'.format(os.environ['SUMO_HOME']),
            '-n', net_path,
            '-d', f.name,
            '--interval', str(sensors_aggr_frequency),
            '-o', out_fname]
    p = subprocess.Popen(opts)
    p.communicate()
    p.wait()
    if p.returncode != 0:
        print('# WARNING: mapDetector termined with errors.')
        return {}
    # get the sensors that have not been mapped because over the road network (execute a dummy simulation)
    over_ = get_detectors_beyond_lanes(net_path, out_fname)
    mapping = {}
    if len(over_) > 0:
        # Override the sensors file definition by removing the sensors that are placed over the road network
        sensors_to_remove = list(over_.keys())
        with open(out_fname) as fd:
            doc = xmltodict.parse(fd.read())
        allowed_sensors = list(
            filter(lambda sensor: sensor['@id'] not in sensors_to_remove, doc['additional']['inductionLoop']))
        with open(out_fname, 'w') as outf:
            sumolib.writeXMLHeader(outf, "$Id$", "additional")
            for sensor in allowed_sensors:
                sensor_id = sensor['@id']
                lane_id = sensor['@lane']
                pos = sensor['@pos']
                sensor_file = sensor['@file']
                freq = sensor['@freq']
                mapping[sensor_id] = lane_id
                outf.write(
                    f"  <inductionLoop id=\"{sensor_id}\" lane=\"{lane_id}\" pos=\"{pos}\" file=\"{sensor_file}\" freq=\"{freq}\"/>\n")
            outf.write("</additional>\n")

        return mapping
    return {}


def traffic_data_to_sumo_edgedata_count(sumo_net, df, timestamps_list, ts_sumo_list, fname="interpolated.edgedata.xml"):
    """
    Generate edgedata file that can be provided to RouteSampler tool in SUMO to calibrate traffic

    This method takes in input the DF, where the index column contains the time stamps, the columns the
    SUMO edges ID where the traffic count is observed, and the values of the table is the traffic count
    (that is, the amount of vehicles). The output is written to an output xml file
    :param df: the DATAFRAME
    :param timestamps_list: list of Timestamps (index of df)
    :param ts_sumo_list: list of timestamps in seconds
    :param fname: output file
    :return:
    """
    fd = open(fname, "w")
    sumolib.xml.writeHeader(fd, "$Id$", "meandata")  # noqa

    for idx, ts in enumerate(timestamps_list[:-1]):
        ts_sumo = ts_sumo_list[idx]
        ts_sumo_next = ts_sumo_list[idx + 1]
        sub_df = df.loc[ts]
        fd.write(f'\n   <interval begin="{ts_sumo}" end="{ts_sumo_next}">\n')

        for idx in sub_df.items():
            edge_id = sumo_net.getLane(idx[0]).getEdge().getID()
            traf_count = idx[1]
            if traf_count is None or not np.isfinite(traf_count):
                traf_count = 0
            fd.write('      <edge id="%s" entered="%d"/>\n' % (edge_id, traf_count))
        fd.write(f' </interval>\n')
    fd.write("</meandata>\n")
    fd.close()


def generate_edgedata_add(filename, edgedata_specification: List[EdgeDataAdditional]) -> None:
    """

    :param filename:
    :param edgedata_specification:
    :param edgedata_dict: id: ID of the edgedata, value:
    :return:
    """
    edgedata = """    <edgeData id="{the_id}" file="{fname}" begin="{begin}" end="{end}" excludeEmpty="{excludeEmpty}"/>\n"""
    with open(filename, "w", encoding="utf8") as outfile:
        sumolib.xml.writeHeader(outfile, root="additional")
        for ed in edgedata_specification:
            outfile.write(
                edgedata.format(
                    the_id=ed.ID,
                    fname=ed.fname,
                    begin=str(ed.begin),
                    end=str(ed.end),
                    excludeEmpty=str(ed.excludeEmpty)
                )
            )
        outfile.write("</additional>\n")


def write_edgedata_add_to_file(output_path, ts_sumo: list[int]) -> None:
    """
    Write the edgedata additional XML file for the given the list of aggregation instants (in ts_sumo).

    :param output_path: where the output XML FILE is written
    :param ts_sumo: the list of aggregation instants
    :return:
    """
    ll = []
    for i in range(len(ts_sumo) - 1):
        ll.append(
            EdgeDataAdditional(f"{ts_sumo[i]}_to_{ts_sumo[i + 1]}",
                               f'edgedata_{ts_sumo[i]}_to_{ts_sumo[i + 1]}.out.xml',
                               ts_sumo[i],
                               ts_sumo[i + 1], True))
    generate_edgedata_add(os.path.join(output_path, 'edgedata.add.xml', ), ll)


def get_coords_df(df_day, train_df, test_df) -> (pd.DataFrame, pd.DataFrame, set[str]):
    """
    Teturn two separate DATAFRAME, each containing three columns: id,lon,lat. Respectively, these are the ID of the
    sensor, the longitude, the latitude for the sensor. The two dataframes are for training and test set respectively.
    A third parameter is also returned: a list of the ID of sensors that have not been mapped (for instance, there is no
    coordinates associated with the sensor)

    :param df_day:
    :param train_df:
    :param test_df:
    :return:
    """
    coords_df = df_day.filter(['source.id', 'lat', 'lang'], axis=1)
    coords_df.columns = ['id', 'lon', 'lat']
    coords_df = coords_df.drop_duplicates(subset=['id'])
    coords_df = coords_df.dropna(axis=0)
    coords_df = coords_df.reset_index(drop=True)

    train_coords_df = coords_df[coords_df['id'].isin(train_df.columns)]
    test_coords_df = coords_df[coords_df['id'].isin(test_df.columns)]

    unmapped_train = set(train_df.columns).difference(set(train_coords_df.id))
    unmapped_test = set(test_df.columns).difference(set(test_coords_df.id))

    return train_coords_df, test_coords_df, unmapped_train.union(unmapped_test)


def main_traintest_kfold(sumo_net_path,
                         dataset_csv_path,
                         days_subset,
                         output_path='output',
                         aggr_frequency_seconds=300,
                         n_folds=3,
                         data_scaling_factor=3) -> None:
    """

    This function splits a given traffic dataset into multiple training and test sets using k-fold cross-validation.
    It processes traffic sensor data to create aggregated datasets, performs resampling, and generates SUMO
    edge data files and sensor definitions for each fold in the cross-validation process.

    :param sumo_net_path (str): The file path to the SUMO network file (usually .net.xml), used to extract sensor and lane information.
    :param dataset_csv_path (str): The file path to the CSV or pickle file that contains the raw traffic dataset. The file is expected to have columns with timestamped traffic data aggregated by sensors.
    :param output_path (str, optional, default 'output'): The directory where the output files (including the training and testing datasets, sensor mappings, and SUMO-related files) will be saved.
    :param aggr_frequency_seconds (int, optional, default 300): The frequency in seconds for data aggregation (e.g., 300 seconds = 5 minutes). Determines how the dataset will be resampled.
    :param n_folds (int, optional, default 3): The number of folds to use in k-fold cross-validation. The dataset will be split into n_folds subsets, and each subset will be used as a test set once.
    :return:
    """
    # days_subset = ["2023-11-30", "2023-12-01", "2023-12-02", "2023-12-03"]

    full_df = pd.read_csv(dataset_csv_path)
    # full_df = pd.read_pickle('bxl_full.pkl')

    kf = KFold(n_splits=n_folds)
    # data_scaling_factor = 3

    # string to datetime
    full_df['_start_timestamp'] = pd.to_datetime(full_df['_start_timestamp'], format='ISO8601')
    all_days = sorted(set(get_all_days(full_df)))

    sumo_net = sumolib.net.readNet(sumo_net_path)

    vtypes = ['light']  # , 'heavy']
    for vtype in vtypes:
        for day in all_days:
            if str(day) not in days_subset:
                continue
            # Extract only the rows related to the current day
            df_day = extract_by_day(str(day), full_df)
            # extract for the specified class of vehicle (either light or heavy)
            df_day_vtype = df_day[df_day['_aggregation_id'].str.contains(vtype)]
            if len(df_day_vtype) <= 0:
                continue
            # resample the dataset so that the time interval between consecutive samples is of [resample_str]min
            resample_str = f"{int(aggr_frequency_seconds / 60)}min"
            resampled = resample_dataset(df_day_vtype, resample_str)
            table_dataset = dataset_to_table(resampled)  # dataset to table. ROWS: time stamps COLUMNS: sensors

            day_sensors = np.array(table_dataset.columns)
            all_splits = kf.split(day_sensors)

            for i, (train_index, test_index) in enumerate(all_splits):
                print('Extracting day {}, fold #{}'.format(day, i))

                train_sensors = day_sensors[train_index]
                test_sensors = day_sensors[test_index]

                train_df = table_dataset.filter(train_sensors)
                test_df = table_dataset.filter(test_sensors)

                # train_df = table_dataset.sample(frac=perc_train, random_state=200, axis=1)
                # test_df = table_dataset.drop(train_df.columns, axis=1)

                train_coords, test_coords, unmapped = get_coords_df(df_day, train_df, test_df)
                train_df = train_df.drop(unmapped, axis=1, errors='ignore')
                test_df = test_df.drop(unmapped, axis=1, errors='ignore')

                train_df = train_df.apply(lambda x: (x / data_scaling_factor).astype(int))
                test_df = test_df.apply(lambda x: (x / data_scaling_factor).astype(int))

                train_path = os.path.join(output_path, vtype, "{}_fold{}".format(str(day), i), 'train')
                test_path = os.path.join(output_path, vtype, "{}_fold{}".format(str(day), i), 'test')
                os.makedirs(os.path.join(train_path), exist_ok=True)
                os.makedirs(os.path.join(test_path), exist_ok=True)

                lane_sensors_mapping_train = write_sensors_definition_to_file(sumo_net_path,
                                                                              train_coords,
                                                                              out_fname=os.path.join(train_path,
                                                                                                     "det.add.xml"),
                                                                              sensors_aggr_frequency=aggr_frequency_seconds)

                lane_sensors_mapping_test = write_sensors_definition_to_file(sumo_net_path,
                                                                             test_coords,
                                                                             out_fname=os.path.join(test_path,
                                                                                                    "det.add.xml"),
                                                                             sensors_aggr_frequency=aggr_frequency_seconds)

                train_coords_df = train_coords[train_coords['id'].isin(list(lane_sensors_mapping_train.keys()))]
                test_coords_df = test_coords[test_coords['id'].isin(list(lane_sensors_mapping_test.keys()))]

                train_df = train_df.drop(set(train_df.columns).difference(set(lane_sensors_mapping_train.keys())),
                                         axis=1)
                test_df = test_df.drop(set(test_df.columns).difference(set(lane_sensors_mapping_test.keys())), axis=1)

                df_train_mapping = pd.DataFrame.from_dict(lane_sensors_mapping_train, orient="index",
                                                          columns=['lane_id'])
                df_train_mapping.index.name = "sensor_id"
                df_train_mapping.to_csv(os.path.join(train_path, 'sensors_edges_mapping.csv'), sep=";")

                df_test_mapping = pd.DataFrame.from_dict(lane_sensors_mapping_test, orient="index", columns=['lane_id'])
                df_test_mapping.index.name = "sensor_id"
                df_test_mapping.to_csv(os.path.join(test_path, 'sensors_edges_mapping.csv'), sep=";")

                train_coords_df.to_csv(os.path.join(train_path, 'det_coordinates.csv'), sep=';')
                test_coords_df.to_csv(os.path.join(test_path, 'det_coordinates.csv'), sep=';')
                train_df.to_csv(os.path.join(train_path, 'count_dataset.csv'), sep=';')
                test_df.to_csv(os.path.join(test_path, 'count_dataset.csv'), sep=';')

                all_ts = table_dataset.index.to_list()
                ts_sumo = [int((ts_other - all_ts[0]).total_seconds()) for ts_other in all_ts]
                write_edgedata_add_to_file(train_path, ts_sumo)
                write_edgedata_add_to_file(test_path, ts_sumo)
                traffic_data_to_sumo_edgedata_count(sumo_net,
                                                    train_df.rename(columns=lane_sensors_mapping_train),
                                                    all_ts,
                                                    ts_sumo,
                                                    fname=os.path.join(train_path, "interpolated.edgedata.xml"))
                traffic_data_to_sumo_edgedata_count(sumo_net,
                                                    test_df.rename(columns=lane_sensors_mapping_test),
                                                    all_ts,
                                                    ts_sumo,
                                                    fname=os.path.join(test_path, "interpolated.edgedata.xml"))


def get_day_df(dataset_csv_path, day="2023-11-15"):
    full_df = pd.read_csv(dataset_csv_path)
    # string to datetime
    full_df['_start_timestamp'] = pd.to_datetime(full_df['_start_timestamp'], format='ISO8601')

    # Extract only the rows related to the input day
    df_day = extract_by_day(str(day), full_df)
    return df_day


def get_counts_and_sensor_coords(df_day, aggr_frequency_seconds=300, vtype="light"):
    """
    Alternative entry point. THis is useful when you want to return the traffic counts (table) and the coordinates of
    the sensors as two DATAFRAMES

    :param df_day:
    :param aggr_frequency_seconds:

    :param vtype:
    :return:
    """
    # extract for the specified class of vehicle (either light or heavy)
    df_day_vtype = df_day[df_day['_aggregation_id'].str.contains(vtype)]
    if len(df_day_vtype) <= 0:
        return pd.DataFrame(), pd.DataFrame()
    # sensors coordinates DF.
    coords_df = df_day_vtype.filter(['source.id', 'lat', 'lang'], axis=1)
    coords_df.columns = ['id', 'lon', 'lat']
    coords_df = coords_df.drop_duplicates(subset=['id'])
    coords_df = coords_df.dropna(axis=0)
    coords_df = coords_df.reset_index(drop=True)

    # resample the dataset so that consecutive samples are at a distance of [resample_str]
    resample_str = f"{int(aggr_frequency_seconds / 60)}min"
    resampled = resample_dataset(df_day_vtype, resample_str)
    table_dataset = dataset_to_table(resampled)  # dataset to table. ROWS: time stamps COLUMNS: sensors

    # remove sensors that have not been assigned to any lane
    unmapped = list(set(table_dataset.columns).difference(set(coords_df.id)))
    table_dataset = table_dataset.drop(unmapped, axis=1)

    return table_dataset, coords_df


def main_notraintest(sumo_net_path, dataset_path, output_path="output", aggr_interval=3600, day="2023-11-15"):
    day_df = get_day_df(dataset_path, day=day)
    os.makedirs(output_path, exist_ok=True)

    counts_df, coords_df = get_counts_and_sensor_coords(day_df, aggr_frequency_seconds=aggr_interval, vtype="light")

    mapping = VirtualSensorsUtil.save_virtual_sensors_to_file(coords_df,
                                                              sumo_net_path,  # os.environ['BXL_NET'],
                                                              aggr_interval,
                                                              det_add_fname=os.path.join(output_path, "det.add.xml"))

    df_mapping = pd.DataFrame.from_dict(mapping, orient="index",
                                        columns=['lane_id'])
    df_mapping.index.name = "sensor_id"
    df_mapping.to_csv(os.path.join(output_path, 'sensors_edges_mapping.csv'), sep=";")

    counts_df = counts_df.drop(columns=counts_df.columns.difference(mapping.keys()))
    counts_df.to_csv(os.path.join(output_path, "count_dataset.csv"), sep=";")

    coords_df = coords_df[coords_df['id'].isin(mapping.keys())]
    coords_df.to_csv(os.path.join(output_path, "det_coordinates.csv"), sep=";")

    all_ts = counts_df.index.to_list()
    ts_sumo = [int((ts_other - all_ts[0]).total_seconds()) for ts_other in all_ts]
    write_edgedata_add_to_file(output_path, ts_sumo)


def to_namur_format(df,
                    vehicle_type="light",
                    bxl_pbf_path="/Users/dguastel/Desktop/WORK/ULB/TORRES/models/REPOSITORY/bxl/osm/bxl_map.pbf",
                    output_file="output.parquet"):
    """
    Convert the data from brussels in a format that can be used with NamurScenarioPreparer project

    To convert OSM to PBF, run osmconvert OSM_FILE -o=PBF_FILE
    :param vehicle_type: can be either LIGHT or HEAVY
    :param bxl_pbf_path:
    :param df_filt: the input parquet file containing the data from BXL. This should be the parquet
    :param output_file:
    :return:
    """
    if vehicle_type == 'heavy':
        df_filt = df[df['_aggregation_id'].str.contains('heavy', na=False)].copy(deep=True)
    else:
        df_filt = df[df['_aggregation_id'].str.contains('light', na=False)].copy(deep=True)

    df_filt["count"] = (df_filt["count"] / 3).astype(int)

    osm = OSM(bxl_pbf_path)
    nodes, edges = osm.get_network(nodes=True, network_type='driving')

    coords_df = df_filt[['source.id', 'lat', 'lang']]
    coords_df.set_index('source.id', inplace=True)
    coords_df = coords_df[~coords_df.index.duplicated(keep='first')]
    coords_df.insert(2, 'osm_id', None)

    osm_geometry = edges[['u', 'v', 'geometry']]

    # https://stackoverflow.com/questions/56520780/how-to-use-geopanda-or-shapely-to-find-nearest-point-in-same-geodataframe
    if not os.path.exists('bxl_full_coords_df.pkl'):
        pbar = tqdm.tqdm(total=len(coords_df))
        for index, row in coords_df.iterrows():
            pbar.update(1)
            pp = Point(row.lang, row.lat)
            dist_fun = lambda osm_row: ((osm_row.u, osm_row.v),
                                        distance((osm_row.geometry.centroid.y, osm_row.geometry.centroid.x),
                                                 (pp.x, pp.y)).m)
            distances_dict = dict(osm_geometry.apply(dist_fun, axis=1).to_list())
            nearest_edge = min(distances_dict, key=distances_dict.get)

            coords_df.loc[index, 'osm_id'] = "{}_{}_0".format(nearest_edge[0], nearest_edge[1])
        pbar.close()
        coords_df = coords_df.drop_duplicates(subset=["osm_id"], keep="first")
        with open("bxl_full_coords_df.pkl", "wb") as f:
            pickle.dump(coords_df, f)
    else:
        with open("bxl_full_coords_df.pkl", "rb") as f:
            coords_df = pickle.load(f)

    coords_df = coords_df.drop_duplicates(subset=["osm_id"], keep="first")
    coords_df['device.id'] = coords_df.index
    coords_df = coords_df[['device.id', 'osm_id']]

    merged_df = df_filt.merge(coords_df, left_on="source.id", right_on="device.id")

    merged_df.rename(columns={"_start_timestamp": "timestamp", "count": "count_long"}, inplace=True)
    merged_df['count_short'] = 0
    merged_df['timestamp'] = merged_df['timestamp'].astype('datetime64[ns]')

    merged_df['ts'] = merged_df['timestamp'].apply(lambda d: d.date())
    merged_df['time'] = merged_df['timestamp'].apply(lambda d: "{}_{}".format(d.date(), d.time().hour))

    merged_df = merged_df[['count_long', 'count_short', 'osm_id', 'ts', 'timestamp', 'time']]
    merged_df.to_parquet(output_file)


def main_namur_format(bxl_pbf_path, dataset_csv_path, days_list, out_folder):
    all_bxl_data = pd.read_csv(dataset_csv_path)
    for day_str in days_list:
        df_day = extract_by_day(day_str, all_bxl_data)

        os.makedirs(out_folder, exist_ok=True)
        to_namur_format(df_day,
                        bxl_pbf_path=bxl_pbf_path,
                        output_file=os.path.join(out_folder, "bxl_{}_namurFormat.parquet".format(day_str)))


def parse_args(args):
    parser = argparse.ArgumentParser()
    # parser.add_argument("-p", type=str, dest="scenario_path", required=True)
    parser.add_argument("-d", type=str, dest="anpr_dataset_file", required=True)
    parser.add_argument("-n", type=str, dest="sumo_net_file", required=True)
    parser.add_argument("-o", type=str, dest="out_folder", required=True)
    parser.add_argument('-t', type=str, required=True, dest='op_type', help="NOTRAINTEST, KFOLD or TO_NAMUR")
    return parser.parse_args(args)


def main(configuration):
    if configuration['op_type'] == "NOTRAINTEST":
        # sumo_net_path, dataset_path, aggr_interval=3600, day="2023-11-15"
        for day in configuration['days_list'].split(' '):
            main_notraintest(sumo_net_path=configuration['sumo_net_file'],
                             dataset_path=configuration['anpr_dataset_file'],
                             output_path=configuration['out_folder'],
                             aggr_interval=int(configuration['aggr_freq']),
                             day=day)
        # TODO must be fixed
    elif configuration['op_type'] == "KFOLD":
        main_traintest_kfold(sumo_net_path=configuration['sumo_net_file'],
                             dataset_csv_path=configuration['anpr_dataset_file'],
                             output_path=configuration['out_folder'],
                             days_subset=configuration['days_list'].split(' '),
                             n_folds=int(configuration['nfolds']),
                             aggr_frequency_seconds=int(configuration['aggr_freq']))
    elif configuration['op_type'] == "TONAMUR":
        main_namur_format(bxl_pbf_path=configuration['pbf_path'],
                          dataset_csv_path=configuration['anpr_dataset_file'],
                          days_list=configuration['days_list'].split(' '),
                          out_folder=configuration['out_folder'])
    else:
        print('Wrong op code')


if __name__ == '__main__':
    with open(sys.argv[1], "rb") as f:
        configuration = tomli.load(f)
        main(configuration)
