from optparse import OptionParser

import pandas as pd
import dateutil.parser

"""
Example usage: python DayExtractor.py -d data_bxl_60min_27apr23.csv -s 2023-04-27 -o out.csv

where data_bxl_60min_27apr23.csv is the dataset scraped from Brussel Mobility openAPI 
"""


def extract_by_day(date_str, df):
    """

    :param date_str: Date in ISO8601 format (ex: 2023-10-28T15:06:15). Do NOT include 'Z' at the end
    :param df:
    :return:
    """
    df_tmp = df.copy(deep=True)
    date = dateutil.parser.isoparse(date_str).date()

    start_times = df['start_time'].to_list()
    df_tmp['start_date'] = list(map(lambda d: d.date(), start_times))

    return df_tmp.loc[df_tmp['start_date'] == date]


def main():
    optParser = OptionParser()

    optParser.add_option("-d", "--dataset_fname", dest="dataset_fname")
    optParser.add_option("-s", "--date", dest="date")
    optParser.add_option("-o", "--output_fname", dest="output_fname")

    (options, args) = optParser.parse_args()

    if not options.dataset_fname or not options.output_fname or not options.date:
        optParser.print_help()
        optParser.error('Arguments not provided')

    df = pd.read_csv(options.dataset_fname, sep=";")
    df['start_time'] = pd.to_datetime(df['start_time'], format='ISO8601')
    df['end_time'] = pd.to_datetime(df['end_time'], format='ISO8601')

    ext_df = extract_by_day(options.date, df)
    ext_df.to_csv(options.output_fname, sep=";")


if __name__ == '__main__':
    main()
