import datetime
import sys
import time
from optparse import OptionParser

import requests
import schedule as schedule

import config
import os
import pandas as pd
import numpy as np


class BXLMobiliteService:
    hour_delta = False

    @classmethod
    def get_bxlmobilite_sensors_position(cls, list_sensors=None):
        """
        Invoke Bruxelles Mobilité API to get the position (in lat/lon) of the available traffic counting devices deployed
        into Brussels city. The function output a CSV file that can be used with "mapDetectors.py" tool:

        >> python <SUMO_HOME>/tools/detector/mapDetectors.py -n net.net.xml -d counting_devices_pos.csv -o det.add.xml
        :param list_sensors: the list of sensors to be included in the output file
        :param output_fname:
        :return:
        """
        output_fname = os.path.join(config.output_path, 'counting_devices_pos.csv')
        columns = ['id', 'lat', 'lon']
        df_out = pd.DataFrame(columns=columns)
        try:
            data = requests.get(
                f"http://data.mobility.brussels/traffic/api/counts/?request=devices&outputFormat=json&interval={config.interval}")
            all_data = data.json()['features']
        except Exception as _:
            return df_out

        for sensor_dict in all_data:
            coordinates = list(reversed(sensor_dict['geometry']['coordinates']))
            sensor_name = sensor_dict['properties']['traverse_name']

            if list_sensors is not None:
                if sensor_name not in list_sensors:
                    continue

            if sensor_name in config.unavailable_sensors or \
                    (not coordinates[0] or not coordinates[1]) or \
                    (not np.isfinite(coordinates[0]) or not np.isfinite(coordinates[1])):
                continue
            record = pd.DataFrame([{'id': sensor_name, 'lat': coordinates[0], 'lon': coordinates[1]}])
            df_out = pd.concat([df_out, record], axis=0, ignore_index=True)
        df_out.to_csv(output_fname, mode='w', sep=';', index=False, header=True)
        return df_out

    @classmethod
    def write_dummy_values_to_file(cls, output_fname, now_time, sensor_name):
        dummy_values = {
            'qPKW': -50,
            'vPKW': -50,
            'occupancy': -50,
            'start_time': now_time,
            'end_time': now_time,
            'Detector': sensor_name
        }
        df = pd.DataFrame.from_dict([dummy_values], orient='columns')
        df.columns = ['qPKW', 'vPKW', 'occupancy', 'start_time', 'end_time', 'Detector']
        df.to_csv(output_fname, sep=";", mode='a', header=True if not os.path.isfile(output_fname) else False)

    @classmethod
    def get_bxlmobilite_data(cls):
        """
        Get data from BXL mobilité. The output CSV can be used with flowrouter.py tool (SUMO) so to generate
        traffic routes based on realistic flow data.

        See: https://data.mobility.brussels/traffic/api/counts/
        :param output_fname:
        :param interval: the time interval at which this method is being invoked (in minutes, possible options are 1,15,60)
        """
        output_fname = os.path.join(config.output_path, 'dataset.csv')

        now_time = datetime.datetime.now()
        print(
            f"- [t: {config.interval}] Getting Data from BXL Mobilité. Time: {now_time}. Output: {output_fname}")

        try:
            data = requests.get(
                f"http://data.mobility.brussels/traffic/api/counts/?request=live&outputFormat=json&interval={config.interval}")
            all_data = data.json()['data']
        except Exception as _:
            print(
                f"- [t: {config.interval}] Failed at Time: {now_time}. Output: {output_fname}")
            cls.write_dummy_values_to_file(output_fname, now_time, -50)
            return

        for sensor_name in all_data:
            if sensor_name in config.unavailable_sensors:
                continue
            sensor_data = all_data[sensor_name]['results'][f'{config.interval}m']

            if not sensor_data['t1']['count']:
                cls.write_dummy_values_to_file(output_fname, now_time, sensor_name)
                continue

            # Why t1? According to the BXL Mobilité API:
            # "Generally the last two timestamp values are given. t1 is the last value, t2 is the timestamp
            # before the last one."
            df = pd.DataFrame.from_dict([sensor_data['t1']])
            df['id'] = sensor_name
            # rename columns so that the output file can be used with SUMO's flowrouter.py tool
            # See https://sumo.dlr.de/docs/Demand/Routes_from_Observation_Points.html#computing_detector_types
            df.columns = ['qPKW', 'vPKW', 'occupancy', 'start_time', 'end_time', 'Detector']
            df.to_csv(output_fname, sep=";", mode='a', header=True if not os.path.isfile(output_fname) else False)

    @classmethod
    def collect_data_from_bxl_mobilite(cls):
        """
        Invoke Bruxelles Mobilité service and collect data each [interval] minutes, and append the collected data to a
        csv file
        :return:
        """
        cls.get_bxlmobilite_data()
        if config.time_horizon <= 0:
            return
        time_change = datetime.timedelta(minutes=config.time_horizon)
        if cls.hour_delta:
            time_change = datetime.timedelta(hours=config.time_horizon)

        # define when the service should terminate scraping data from BXL mobility
        until_time = datetime.datetime.now() + time_change
        print(f'Stopping at {until_time}')

        # finally, repeat for N hours/minutes/seconds the invocation of BXL mobilité service to get data from sensors
        schedule.every(config.interval).minutes.until(until_time).do(cls.get_bxlmobilite_data)

        while True:
            schedule.run_pending()
            if not schedule.jobs:
                break
            time.sleep(1)

    @classmethod
    def parse_args(cls):
        optParser = OptionParser()
        optParser.add_option("-i", "--interval", dest="interval",
                             help="interval at which BXL mobilité API should be invoked (possible values are 1,15,60, in minutes, MANDATORY).")
        optParser.add_option("-H", "--hours", dest="is_hours", default=False,
                             help="if TRUE, the value of [duration] parameter is considered in HOURS, otherwise in MINUTES (MANDATORY).")
        optParser.add_option("-t", "--duration", dest="duration",
                             help="the duration of the data retrieval (by default in minutes, MANDATORY)")
        optParser.add_option("-o", "--output", dest="output_path", default="output/", help="the output PATH.")

        (options, args) = optParser.parse_args()

        if not options.interval or not options.duration:
            optParser.print_help()
            sys.exit()

        if options.interval:
            config.interval = int(options.interval)
        if options.is_hours:
            cls.hour_delta = True
        if options.duration:
            config.time_horizon = int(options.duration)
        if options.output_path:
            config.output_path = options.output_path

    @classmethod
    def main(cls):
        cls.parse_args()
        if not os.path.exists(config.output_path):
            os.makedirs(config.output_path)
        cls.get_bxlmobilite_sensors_position()
        cls.collect_data_from_bxl_mobilite()


if __name__ == '__main__':
    BXLMobiliteService.main()
