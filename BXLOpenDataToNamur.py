import sys
from optparse import OptionParser

import numpy as np
import pandas as pd
import requests
import tqdm
from geopy.distance import distance
from pyrosm.pyrosm import OSM
from shapely.geometry import Point

import DayExtractor


class BXLOpenDataToNamur:
    @classmethod
    def get_bxlmobilite_sensors_position(cls, output_fname=None, interval=60):
        """
        Invoke Bruxelles Mobilité API to get the position (in lat/lon) of the available traffic counting devices deployed
        into Brussels city. This output a CSV file that can be used with "mapDetectors.py" tool:

        >> python <SUMO_HOME>/tools/detector/mapDetectors.py -n net.net.xml -d counting_devices_pos.csv -o det.add.xml
        :param output_fname:
        :param interval:
        :return:
        """
        # output_fname = os.path.join(config.output_path, 'counting_devices_pos.csv')
        columns = ['id', 'lat', 'lon']
        df_out = pd.DataFrame(columns=columns)
        try:
            data = requests.get(
                f"http://data.mobility.brussels/traffic/api/counts/?request=devices&outputFormat=json&interval={interval}")
            all_data = data.json()['features']
        except Exception as _:
            print('Unable to get data from Brussels Mobility service. Maybe the service is not available?')
            return df_out

        for sensor_dict in all_data:
            coordinates = list(reversed(sensor_dict['geometry']['coordinates']))
            sensor_name = sensor_dict['properties']['traverse_name']

            if (not coordinates[0] or not coordinates[1]) or \
                    (not np.isfinite(coordinates[0]) or not np.isfinite(coordinates[1])):
                continue
            record = pd.DataFrame([{'id': sensor_name, 'lat': coordinates[0], 'lon': coordinates[1]}])
            df_out = pd.concat([df_out, record], axis=0, ignore_index=True)
        if output_fname is not None:
            df_out.to_csv(output_fname, mode='w', sep=';', index=False, header=True)
        return df_out

    @classmethod
    def to_namur_format(cls,
                        sensors_data_df,
                        sensors_pos_df,
                        bxl_pbf_path,
                        output_file="output.parquet"):
        """
        Convert the data from brussels in a format that can be used with NamurScenarioPreparer project

        To convert OSM to PBF, run osmconvert OSM_FILE -o=PBF_FILE

        :param sensors_data_df:
        :param sensors_pos_df:
        :param bxl_pbf_path:
        :param output_file:
        :return:
        """
        merged_df = sensors_data_df.merge(sensors_pos_df, left_on="Detector", right_on="id").drop(columns=['id'])

        osm = OSM(bxl_pbf_path)
        _, edges = osm.get_network(nodes=True, network_type='driving')
        sensors_pos_df.set_index('id', inplace=True)
        sensors_pos_df.insert(1, 'osm_id', None)
        osm_geometry = edges[['u', 'v', 'geometry']]

        # https://stackoverflow.com/questions/56520780/how-to-use-geopanda-or-shapely-to-find-nearest-point-in-same-geodataframe
        pbar = tqdm.tqdm(total=sensors_pos_df.shape[0])
        for index, row in sensors_pos_df.iterrows():
            pbar.update(1)
            pp = Point(row.lon, row.lat)
            dist_fun = lambda osm_row: ((osm_row.u, osm_row.v),
                                        distance((osm_row.geometry.centroid.y, osm_row.geometry.centroid.x),
                                                 (pp.y, pp.x)).m)
            distances_dict = dict(osm_geometry.apply(dist_fun, axis=1).to_list())
            nearest_edge = min(distances_dict, key=distances_dict.get)
            sensors_pos_df.loc[index, 'osm_id'] = "{}_{}_0".format(nearest_edge[0], nearest_edge[1])
        pbar.close()

        sensors_pos_df = sensors_pos_df.drop_duplicates(subset=["osm_id"], keep="first")
        sensors_pos_df['Detector'] = sensors_pos_df.index
        merged_df = merged_df.merge(sensors_pos_df, left_on="Detector", right_on="Detector")

        merged_df.rename(columns={"start_time": "timestamp", "qPKW": "count_long", "vPKW": "speed_long"}, inplace=True)
        merged_df['count_short'] = 0

        merged_df = merged_df[['count_long', 'count_short', 'speed_long', 'osm_id', 'timestamp']]
        merged_df['timestamp'] = merged_df['timestamp'].astype('datetime64[ns]')

        merged_df['ts'] = merged_df['timestamp'].apply(lambda d: d.date())
        merged_df['time'] = merged_df['timestamp'].apply(lambda d: "{}_{}".format(d.date(), d.time().hour))

        merged_df.loc[merged_df['count_long'] < 0, 'count_long'] = 0
        merged_df.loc[merged_df['speed_long'] < 0, 'speed_long'] = 0

        merged_df.to_parquet(output_file)


def main(args):
    usage = "usage: python %prog [options] arg1 arg2\n\n\tExample usage: python %prog -t conf.toml"

    optParser = OptionParser(usage=usage)
    optParser.add_option("-p", "--bxl_pbf_path", dest="bxl_pbf_path")
    optParser.add_option("-s", "--sensors_coords", dest="sensors_coords")
    optParser.add_option("-f", "--dataset_fname", dest="dataset_fname")
    optParser.add_option("-d", "--day_to_extract", dest="day_to_extract")
    optParser.add_option("-o", "--output_file", dest="output_file")

    (cml, _) = optParser.parse_args(args)

    df = pd.read_csv(cml.dataset_fname, sep=";")
    df['start_time'] = pd.to_datetime(df['start_time'], format='ISO8601')
    df['end_time'] = pd.to_datetime(df['end_time'], format='ISO8601')
    BXLOpenDataToNamur.to_namur_format(sensors_data_df=DayExtractor.extract_by_day(cml.day_to_extract, df),
                                       sensors_pos_df=pd.read_csv(cml.sensors_coords, sep=";"),
                                       bxl_pbf_path=cml.bxl_pbf_path,
                                       output_file=cml.output_file)
    print('done!')


if __name__ == '__main__':
    """
    This script allows to convert brussels open data traffic counts into the same format of data that Macq provided for
    Namur. This is for converging to a unique data format that can be then used for calibrating traffic scenarios.
    """
    if len(sys.argv) > 1:
        main(sys.argv[1:])
    else:
        #
        # the PBF file related to the road network. Use osmconvert to convert OSM>pbf
        # bxl_pbf_path = "open_data/scenario/osm/bxl.pbf"
        bxl_pbf_path = "open_data/scenario/osm/bxl_highways.pbf"
        # the file containing the coordinates lat/lon of the sensors (this is obtained by BXLMob service)
        sensors_coords = "open_data/all_counting_devices_pos.csv"
        # the FULL dataset obtained by BXLMob open data
        dataset_fname = "open_data/output_15min_27apr23.csv"
        # the day which data must be extracted
        date = "2023-12-05"
        # the output parquet file
        output_file = "open_data/output/bxlopendata_{}_namurFormat.parquet".format(date)
        main(['-p', bxl_pbf_path,
              '-s', sensors_coords,
              '-f', dataset_fname,
              '-d', date,
              '-o', output_file])
