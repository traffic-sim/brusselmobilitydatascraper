# Possible values are '1', '5', '15', '60' and 'all'. The unit is minutes
# See: https://data.mobility.brussels/traffic/api/counts/
interval = 60  # minutes

# time_horizon = 60  # minutes
time_horizon = 840  # 00->23h in minutes

unavailable_sensors = ['STE_TD3', 'HAL_191', 'SUL62_BHout', 'SUL62_BDin', 'SUL62_BDout', 'SUL62_BGin', 'SUL62_BGout',
                       'SUL62_BHin', 'SB0236_BCout', 'SB0236_BHout', 'SB0246_BAout', 'SB0246_BXout', 'NATO_104',
                       'NATO_204', 'DEL_103']

output_path = 'out/'
