from optparse import OptionParser

import pandas as pd
import dateutil.parser
import os

"""
Example usage: 

> python AllDayExtractor.py -d data_bxl_60min_27apr23.csv -o output/ 

where data_bxl_60min_27apr23.csv is the file containing the data obtained from from Brussel Mobility API:

ref https://data.mobility.brussels/traffic/api/counts/
"""


def extract_and_save_all_days(output_dir, df):
    """

    :param output_dir:
    :param df:
    :return:
    """
    start_times = df['start_time'].to_list()
    df_tmp = df.copy(deep=True)
    df_tmp['start_date'] = list(map(lambda d: d.date(), start_times))

    for date in df_tmp['start_date'].unique().tolist():
        sub_df = df_tmp.loc[df_tmp['start_date'] == date]
        sub_df.to_csv(os.path.join(output_dir, f"dataset_{str(date)}.csv"), sep=";")


def main():
    optParser = OptionParser()

    optParser.add_option("-d", "--dataset_fname", dest="dataset_fname")
    optParser.add_option("-o", "--output_dir", dest="output_dir")

    (options, args) = optParser.parse_args()

    if not options.dataset_fname or not options.output_dir:
        optParser.print_help()
        optParser.error('Arguments not provided')

    if not os.path.exists(options.output_dir):
        os.makedirs(options.output_dir)

    df = pd.read_csv(options.dataset_fname, sep=";")
    df['start_time'] = pd.to_datetime(df['start_time'], format='ISO8601')
    df['end_time'] = pd.to_datetime(df['end_time'], format='ISO8601')

    extract_and_save_all_days(options.output_dir, df)


if __name__ == '__main__':
    main()
