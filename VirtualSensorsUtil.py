import subprocess
import tempfile

import sumolib
import re
import xmltodict
import os
import pandas as pd


def get_detectors_beyond_lanes(net_path, det_add_fname):
    sumo = sumolib.checkBinary("sumo")
    opts = [sumo,
            "-n", net_path,
            '-b', str(0),
            '-e', str(1),
            '-a', det_add_fname]
    p = subprocess.Popen(opts, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    p.wait()
    if len(err) <= 0:
        return {}
    if b'Quitting (on error).' not in err.splitlines():
        return {}
    pattern = re.compile(b"Error: The position of e1Detector '(.*)' lies beyond the lane's '(.*)' end.")
    # Error: The position of e1Detector '298428751' lies beyond the lane's '298428751_0' end.
    match_dict = {}
    for i, line in enumerate(err.splitlines()):
        matches = pattern.findall(line)
        if matches:
            match_dict[matches[0][0].decode()] = matches[0][1].decode()

    # by running the simulation, we generate the detector output file which is useless for the dataset
    os.remove(os.path.join(os.path.dirname(det_add_fname), "detector.out.xml"))
    return match_dict


def save_virtual_sensors_to_file(sensor_coordinates_df: pd.DataFrame,
                                 net_path: str,
                                 aggr_interval_sec: int,
                                 det_add_fname: str = None):
    if det_add_fname is None:
        det_add_fname_tmp = tempfile.NamedTemporaryFile(suffix='.xml')
        det_add_fname = det_add_fname_tmp.name

    f = tempfile.NamedTemporaryFile(suffix='.csv')
    sensor_coordinates_df.to_csv(f.name, sep=';')
    opts = ['python', os.path.join(os.environ['SUMO_HOME'], 'tools', 'detector', 'mapDetectors.py'),
            '-n', net_path,
            '-d', f.name,
            '--interval', str(aggr_interval_sec),
            '-o', det_add_fname]
    p = subprocess.Popen(opts)
    p.communicate()
    p.wait()
    if p.returncode != 0:
        print('# WARNING: mapDetector termined with errors.')
        return []

    over_ = get_detectors_beyond_lanes(net_path, det_add_fname)
    sensors_to_remove = list(over_.keys())
    with open(det_add_fname) as fd:
        doc = xmltodict.parse(fd.read())
    allowed_sensors = list(
        filter(lambda sensor: sensor['@id'] not in sensors_to_remove, doc['additional']['inductionLoop']))

    if len(over_) > 0 and det_add_fname:
        with open(det_add_fname, 'w') as outf:
            sumolib.writeXMLHeader(outf, "$Id$", "additional")
            for sensor in allowed_sensors:
                sensor_id = sensor['@id']
                lane_id = sensor['@lane']
                pos = sensor['@pos']
                sensor_file = sensor['@file']
                freq = sensor['@freq']
                outf.write(
                    f"  <inductionLoop id=\"{sensor_id}\" lane=\"{lane_id}\" pos=\"{pos}\" file=\"{sensor_file}\" freq=\"{freq}\"/>\n")
            outf.write("</additional>\n")

    sensor_lane_mapping = dict(map(lambda elem: (elem['@id'], elem['@lane']), allowed_sensors))
    return sensor_lane_mapping
